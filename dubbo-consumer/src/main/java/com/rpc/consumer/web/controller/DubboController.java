package com.rpc.consumer.web.controller;

import com.rpc.service.DemoService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DubboController {

    @DubboReference(version = "${demo.service.version}")
    private DemoService demoService;

    @GetMapping(value = "/sayHello")
    public String sayHello(@RequestParam(value = "name",defaultValue = "") String name){
        return demoService.sayHello(name);
    }
}
